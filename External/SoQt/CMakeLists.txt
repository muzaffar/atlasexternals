# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building SoQt for ATLAS.
#

# The package's name:
atlas_subdir( SoQt )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# External package(s):
find_package( Qt5 )

# Note: Coin3D is taken from the ATLAS Externals
#find_package( Coin3D )


# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SoQtBuild )

# Extra configuration parameters:
set( _extraConf )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   set( _extraConf -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()


# Set up the build of SoQt:
ExternalProject_Add( SoQt
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   
   GIT_REPOSITORY https://github.com/ric-bianchi/SoQt.git
   GIT_TAG 33321df    
   
   # in order to let CMake find the custom Coin3D instead of the LCG one, we
   # need to pass the path '${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}' to
   # -DCMAKE_PREFIX_PATH
   CMAKE_CACHE_ARGS -DCMAKE_PREFIX_PATH:PATH=${QT5_ROOT};${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DCMAKE_SYSTEM_IGNORE_PATH:STRING=/usr/lib;/usr/lib32;/usr/lib64;/usr/local/lib 
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir} ${_extraConf}

   LOG_CONFIGURE 1 )

ExternalProject_Add_Step( SoQt buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing SoQt into the build area"
   DEPENDEES install )

add_dependencies( SoQt Coin3D )
add_dependencies( Package_SoQt SoQt )

# Install SoQt:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )



# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthAnalysisExternals.
#
#+ External/Boost
+ External/BAT
+ External/Eigen
+ External/FastJet
+ External/FastJetContrib
+ External/GoogleTest
+ External/Lhapdf
#+ External/MCUtils
#+ External/Python
#+ External/ROOT

+ External/CheckerGccPlugins
#+ External/yampl

#possible additional externals for core athena
#+ External/Blas
+ External/CLHEP
#+ External/GPerfTools
#+ External/Gdb
#+ External/HepMCAnalysis
#+ External/Lapack
#+ External/MKL
#+ External/dSFMT


- .*
